create table EMP_Employee (
	uuid_ VARCHAR(75) null,
	employeeId LONG not null primary key,
	name VARCHAR(75) null,
	address VARCHAR(75) null,
	createdBy LONG
);