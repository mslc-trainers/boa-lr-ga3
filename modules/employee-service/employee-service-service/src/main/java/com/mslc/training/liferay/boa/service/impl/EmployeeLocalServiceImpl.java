/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.mslc.training.liferay.boa.service.impl;

import com.liferay.blogs.model.BlogsEntry;
import com.liferay.blogs.service.BlogsEntryLocalServiceUtil;
import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.transaction.Transactional;
import com.mslc.training.liferay.boa.model.Employee;
import com.mslc.training.liferay.boa.service.base.EmployeeLocalServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * The implementation of the employee local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.mslc.training.liferay.boa.service.EmployeeLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EmployeeLocalServiceBaseImpl
 */
@Component(
        property = "model.class.name=com.mslc.training.liferay.boa.model.Employee",
        service = AopService.class
)
public class EmployeeLocalServiceImpl extends EmployeeLocalServiceBaseImpl {

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Use <code>com.mslc.training.liferay.boa.service.EmployeeLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>com.mslc.training.liferay.boa.service.EmployeeLocalServiceUtil</code>.
     */

    public List<Employee> getEmployeesByName(String name) {
        return employeePersistence.findByName(name);
    }


    @Transactional
    public Employee createEmployee(long id, String name, String address, ServiceContext ctx) {

        Employee emp = employeePersistence.create(id);
        emp.setName(name);
        emp.setAddress(address);


//        roleLocalService.getRol

//        User user = UserLocalServiceUtil.createUser(id);
        User user = userLocalService.createUser(0);

//        BlogsEntryLocalServiceUtil.

//        BlogsEntry b = blogsEntryLocalService.createBlogsEntry(0);
//
//        blogsEntryLocalService.updateBlogsEntry(b);
//

//        assetEntryLocalService.


        return emp;

    }

}
