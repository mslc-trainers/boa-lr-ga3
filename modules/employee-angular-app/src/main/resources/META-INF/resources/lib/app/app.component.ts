import { Component } from '@angular/core';

export class Hero {
	id: number;
	name: string;
}

@Component({
	templateUrl: '/o/employee-angular-app/lib/app/app.component.html'

})
export class AppComponent {
	hero: Hero = {
		id: 1,
		name: 'Windstorm'
	};
	title = 'Tour of Heroes';
}
