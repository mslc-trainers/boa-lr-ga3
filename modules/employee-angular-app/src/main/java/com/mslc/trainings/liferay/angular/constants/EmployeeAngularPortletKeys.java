package com.mslc.trainings.liferay.angular.constants;

/**
 * @author shakir
 */
public class EmployeeAngularPortletKeys {

	public static final String EmployeeAngular = "employeeangular";

}