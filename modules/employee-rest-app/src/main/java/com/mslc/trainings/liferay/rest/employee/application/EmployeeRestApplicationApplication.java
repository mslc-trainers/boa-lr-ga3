package com.mslc.trainings.liferay.rest.employee.application;

import com.mslc.training.liferay.boa.model.Employee;
import com.mslc.training.liferay.boa.service.EmployeeLocalServiceUtil;
import com.mslc.trainings.liferay.rest.employee.application.vo.EmployeeVO;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author shakir
 */
@Component(
        property = {
                JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/employees",
                JaxrsWhiteboardConstants.JAX_RS_NAME + "=Employees.Rest",
                "auth.verifier.guest.allowed=false"
        },
        service = Application.class
)
public class EmployeeRestApplicationApplication extends Application {

    public Set<Object> getSingletons() {
        return Collections.<Object>singleton(this);
    }

    @GET
    @Produces("application/json")
    public List<EmployeeVO> handleGetEmployees() {

        List<Employee> employeeList = EmployeeLocalServiceUtil.getEmployees(0, 100);

        return
                employeeList
                        .stream()
                        .map(x -> {
                            EmployeeVO vo = new EmployeeVO();
                            vo.setEmployeeId(x.getEmployeeId());
                            vo.setName(x.getName());
                            vo.setAddress(x.getAddress());
//                            BeanPropertiesUtil.cop

                            return vo;
                        }).collect(Collectors.toList());

    }


}
