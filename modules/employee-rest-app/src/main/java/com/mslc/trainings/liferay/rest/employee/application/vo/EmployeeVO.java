package com.mslc.trainings.liferay.rest.employee.application.vo;


//@Data
public class EmployeeVO {

    private long employeeId;
    private String name;
    private String address;

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
