<%@ page import="com.mslc.training.liferay.boa.model.Employee" %>
<%@ include file="/init.jsp" %>


<h2>Add Employee</h2>
<hr/>

<%

    Employee employee = (Employee) request.getAttribute("employee");


%>

<portlet:actionURL var="submitEmployeeForm"/>


<form method="POST" action="<%=submitEmployeeForm%>">
    <div class="form-group">
        <label for="employeeId">Id</label>


        <input type="text" id="employeeId" class="form-control" name="<portlet:namespace/>employeeId"
               placeholder="Employee Id" value="<%=(employee == null ? "" : employee.getEmployeeId())  %>">

    </div>


    <div class="form-group">
        <label for="employeeId">Name</label>

        <input type="text" id="name" class="form-control" name="<portlet:namespace/>name"
               value="<%=(employee == null ? "" : employee.getName())  %>"
               placeholder="Name">


    </div>
    <div class="form-group">
        <label for="employeeId">Address</label>
        <input type="text" id="address" class="form-control" name="<portlet:namespace/>address"
               value="<%=(employee == null ? "" : employee.getAddress())  %>"
               placeholder="Address">
    </div>

    <div class="row">
        <div class="col">
            <input type="submit" class="btn btn-primary" value="Submit">
        </div>
    </div>

</form>

