<%@ page import="java.util.List" %>
<%@ page import="java.util.Collections" %>
<%@ page import="com.mslc.training.liferay.boa.model.Employee" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="javax.portlet.MutableRenderParameters" %>
<%@ include file="/init.jsp" %>

<%--<p>--%>
<%--	<b><liferay-ui:message key="employee.caption"/></b>--%>
<%--</p>--%>


<%

    List<Employee> employeeList = (List) request.getAttribute("employeeList");
    if (employeeList == null) {
        employeeList = Collections.emptyList();
    }
    PortletURL editURL = renderResponse.createRenderURL();
%>

<portlet:renderURL var="toAddEmployee">
    <portlet:param name="action" value="toAddEmployee"/>
</portlet:renderURL>

<%--<portlet:renderURL var="editURL">--%>
<%--    <portlet:param name="action" value="edit"/>--%>
<%--</portlet:renderURL>--%>


<h2>Employee Application</h2>
<hr/>
<%--<p>--%>
<%--    User Role(s) :--%>
<%--    <%--%>


<%--        user.getRoles().forEach(x -> {--%>
<%--            System.out.printf(x.getName());--%>

<%--        }); %>--%>

<%--    %>--%>
<%--</p>--%>
<%--<p>--%>

<%--    User Id : <%=user.getUserId()%> |--%>
<%--    User Name : <%=user.getFirstName()%> |--%>

<%--    Site : <%=themeDisplay.getScopeGroup().getName()%>--%>
<%--    | Virtual Instance :  <%=themeDisplay.getCompany().getCompanyId()%>--%>
<%--</p>--%>

<%
    if (themeDisplay.isSignedIn()) {
%>
<div>

    <a href="<%=toAddEmployee%>" class="btn btn-primary">Add</a>

    <p>
        <%=user.getFullName()%>
    </p>
    <p>
        <%=portletName%>
        | <%=scopeGroupId%> | <%=layout.getLayoutId()%>
    </p>

</div>

<% } else {%>

<p>Sign In to Add</p>
<p>
    <%=user.getFullName()%>
</p>

<% } %>


<hr/>
<div>

    <% for (Employee e : employeeList) { %>
    <div class="row">
        <div class="col">
            <%=e.getEmployeeId()%>
        </div>
        <div class="col">
            <%=e.getName()%>
        </div>
        <div class="col">

            <%=e.getAddress()%>
        </div>

        <div>

            <%
                //                editURL.setParameter("employeeId", String.valueOf(e.getEmployeeId()));
//                editURL.setParameter("action", "edit");


                MutableRenderParameters mutableRenderParameters = editURL.getRenderParameters();
                mutableRenderParameters.setValue("employeeId", String.valueOf(e.getEmployeeId()));
                mutableRenderParameters.setValue("action", "edit");

            %>
            <a href="<%=editURL%>">Edit</a>
        </div>

    </div>
    <% } %>
</div>


<script>

    Liferay.on("departmentSelected", function (event) {

        alert("In employee portlet : " + event.departmentId);
    });


</script>