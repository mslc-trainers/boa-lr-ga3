package com.mslc.trainings.liferay.portlet.employee.constants;

/**
 * @author shakir
 */
public class EmployeePortletKeys {

	public static final String EMPLOYEE =
		"com_mslc_trainings_liferay_portlet_employee_EmployeePortlet";

}