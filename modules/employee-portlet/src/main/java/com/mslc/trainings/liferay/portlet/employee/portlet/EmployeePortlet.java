package com.mslc.trainings.liferay.portlet.employee.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.mslc.training.liferay.boa.exception.NoSuchEmployeeException;
import com.mslc.training.liferay.boa.model.Employee;
import com.mslc.training.liferay.boa.service.EmployeeLocalServiceUtil;
import com.mslc.training.liferay.boa.service.persistence.EmployeeUtil;
import com.mslc.trainings.liferay.portlet.employee.constants.EmployeePortletKeys;
import org.osgi.service.component.annotations.Component;

import javax.portlet.*;
import java.io.IOException;
import java.util.List;

/**
 * @author shakir
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.display-category=category.sample",
                "com.liferay.portlet.header-portlet-css=/css/main.css",
                "com.liferay.portlet.instanceable=false",
                "javax.portlet.display-name=Employee",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/view.jsp",
                "javax.portlet.name=" + EmployeePortletKeys.EMPLOYEE,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user",
                "javax.portlet.version=3.0",
                "javax.portlet.supported-public-render-parameter=departmentId"
        },
        service = Portlet.class
)
public class EmployeePortlet extends MVCPortlet {


    private static final Log _log = LogFactoryUtil.getLog(EmployeePortlet.class);

    @Override
    public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {

        ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

//
        PortletSession session  = renderRequest.getPortletSession();
        session.setAttribute("key", "value", PortletSession.APPLICATION_SCOPE);


        String resourcePrimaryKey = themeDisplay.getPortletDisplay().getResourcePK();
        long scopeGroupId = themeDisplay.getScopeGroupId();
        PermissionChecker permissionChecker = themeDisplay.getPermissionChecker();

        boolean result =
                permissionChecker
                        .hasPermission(scopeGroupId, EmployeePortletKeys.EMPLOYEE,
                                resourcePrimaryKey,
                                "ADD_EMPLOYEE");
        _log.info("Result (has rights) " + result);

        List<Employee> listByNames =
                EmployeeLocalServiceUtil
                        .getEmployeesByName("Shakir");
        listByNames.forEach(System.out::println);


        RenderParameters parameters = renderRequest.getRenderParameters();

        String departmentId = parameters.getValue("departmentId");
        _log.info("The departmentId is : " + departmentId);

        String action = parameters.getValue("action");
        if ("toAddEmployee".equalsIgnoreCase(action)) {
            toAddEmployee(renderRequest, renderResponse);
        } else if ("edit".equalsIgnoreCase(action)) {
            toEditEmployee(renderRequest, renderResponse);

        } else {

            List<Employee> employeeList = EmployeeLocalServiceUtil.getEmployees(0, 100);
            renderRequest.setAttribute("employeeList", employeeList);

            super.render(renderRequest, renderResponse);
        }
    }


    @Override
    public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {

        ActionParameters actionParameters = actionRequest.getActionParameters();
        String id = actionParameters.getValue("employeeId");
        String name = actionParameters.getValue("name");
        String address = actionParameters.getValue("address");


        _log.info("Process action is executed : " + id + " -- " + name + " -- " + address);


        Employee emp = null;

        try {
            emp = EmployeeLocalServiceUtil.getEmployee(Long.valueOf(id));
        } catch (NoSuchEmployeeException e) {
            _log.error(e.getMessage());
        } catch (PortalException e) {
            _log.error(e.getMessage());
        }

        try {
            if (emp == null) {
                emp = EmployeeLocalServiceUtil.createEmployee(Long.valueOf(id));
            }

            ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

//        User user = themeDisplay.getUser();
            String resourcePrimaryKey = themeDisplay.getPortletDisplay().getResourcePK();
            long scopeGroupId = themeDisplay.getScopeGroupId();
            PermissionChecker permissionChecker = themeDisplay.getPermissionChecker();

//            boolean result =
//                    permissionChecker
//                            .hasPermission(scopeGroupId, EmployeePortletKeys.EMPLOYEE,
//                                    resourcePrimaryKey,
//                                    "ADD_EMPLOYEE");
//            _log.info("Result (has rights) " + result);

            emp.setName(name);
            emp.setAddress(address);
            emp.setCreatedBy(themeDisplay.getUserId());

//        themeDisplay.isSignedIn()


            EmployeeLocalServiceUtil.updateEmployee(emp);
            _log.info("Updating ...");

            super.processAction(actionRequest, actionResponse);
        } catch (Exception e) {
            _log.error("Error : " + e.getMessage());
        }
    }

    private void toAddEmployee(RenderRequest renderRequest, RenderResponse renderResponse) {
        PortletRequestDispatcher dispatcher = renderRequest
                .getPortletContext()
                .getRequestDispatcher("/employee-form.jsp");
        try {
            dispatcher.forward(renderRequest, renderResponse);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void toEditEmployee(RenderRequest renderRequest, RenderResponse renderResponse) {

        long empId = Long.valueOf(renderRequest.getRenderParameters().getValue("employeeId"));
        try {
            Employee emp = EmployeeLocalServiceUtil.getEmployee(empId);
            renderRequest.setAttribute("employee", emp);
        } catch (PortalException e) {
            e.printStackTrace();
        }


        PortletRequestDispatcher dispatcher = renderRequest
                .getPortletContext()
                .getRequestDispatcher("/employee-form.jsp");
        try {
            dispatcher.forward(renderRequest, renderResponse);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
