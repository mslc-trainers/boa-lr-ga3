<%@ include file="/init.jsp" %>

<p>
    <b><liferay-ui:message key="department.caption"/></b>
</p>


<portlet:renderURL var="selectDepartment">
    <portlet:param name="departmentId" value="101"/>
</portlet:renderURL>

<a onclick="invokeEvent('101'); return false;">Select Department</a>


<script>

    function invokeEvent(deptId) {
        // alert(deptId);
        Liferay.fire("departmentSelected", {
            departmentId: deptId
        })
    }
</script>