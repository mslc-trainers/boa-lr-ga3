package com.mslc.trainings.liferay.boa.department.constants;

/**
 * @author shakir
 */
public class DepartmentPortletKeys {

	public static final String DEPARTMENT =
		"com_mslc_trainings_liferay_boa_department_DepartmentPortlet";

}